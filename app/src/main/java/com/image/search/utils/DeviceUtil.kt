package com.image.search.utils

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.ConnectivityManager
import android.util.Log
import android.widget.ImageView
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


fun isInternetAvailable(context: Context): Boolean {
    val manager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val connection = manager.activeNetworkInfo
    return connection != null && connection.isConnectedOrConnecting
}
fun Activity.isConnectedInternet(): Boolean {
    val connectivityManager =
        this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
    return if (connectivityManager != null) {
        val networkInfo = connectivityManager.activeNetworkInfo
        networkInfo != null && networkInfo.isConnected
    } else {
        false
    }
}
fun ImageView.setImageWithUrl(imageUrl: String){
        val subscription = loadImage(imageUrl)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe({bitmap->
            this.setImageBitmap(bitmap)
        },{

        })
}
fun loadImage(imageUrl : String) : Single<Bitmap> {
    return Single.fromCallable {
    //    val imageURL = "https://images.pexels.com/photos/14769376/pexels-photo-14769376.jpeg"
        val `in` = java.net.URL(imageUrl).openStream()
        val image = BitmapFactory.decodeStream(`in`)
        image
    }
}