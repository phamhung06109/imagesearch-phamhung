package com.image.search.utils.constants

import android.text.Editable
import android.text.TextUtils


class Constant {
    companion object {
        const val DEFAULT_TIMEOUT = 60L
        const val ITEM_PER_PAGE = 15

    }
    enum class ADAPTER_MARGIN_TYPE{
        ALL,
        CENTER,
        TOP,
        LEFT,
        RIGHT
    }


}