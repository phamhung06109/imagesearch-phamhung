package com.image.search.utils

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.image.search.utils.constants.Constant

class MarginItemDecoration(private val space: Int, private val type: Constant.ADAPTER_MARGIN_TYPE) :
    RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        with(outRect) {
            if (type == Constant.ADAPTER_MARGIN_TYPE.ALL) {
//                if (parent.getChildAdapterPosition(view) == 0) {
//
//                }
                top = space
                left = space
                right = space
                bottom = space
            } else if (type == Constant.ADAPTER_MARGIN_TYPE.CENTER){
                if (parent.getChildAdapterPosition(view) % 2 == 0) {
                    right = space
                }else{
                    left = space
                }
                top = space
                bottom = space
            } else if (type == Constant.ADAPTER_MARGIN_TYPE.TOP){
                if (parent.getChildAdapterPosition(view) != 0) {
                    top = space
                }
            }

        }
    }
}