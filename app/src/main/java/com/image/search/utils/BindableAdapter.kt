package com.image.search.utils

interface BindableAdapter<T> {
    fun setData(data : T)
}