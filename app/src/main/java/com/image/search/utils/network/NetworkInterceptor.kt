package com.image.search.utils.network

import android.content.Context
import com.image.search.data.preferences.SharedPreferences
import com.image.search.utils.isInternetAvailable
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NetworkInterceptor@Inject constructor (private var sharedPreferences: SharedPreferences, var context: Context) : Interceptor {

    private val networkEvent: NetworkEvent = NetworkEvent

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request().newBuilder()

        /*
         * We check if there is internet
         * available in the device. If not, pass
         * the networkState as NO_INTERNET.
         * */
        request.header("Authorization", "563492ad6f917000010000019f83d48ffb6d4fb18ef5fa12fd8bcb32")
        request.build()

        val response = chain.proceed(request.build())
   //     Log.e("HEADER",request.build().headers.toString())
        if (!isInternetAvailable(context)) {
            networkEvent.publish(NetworkState.NO_INTERNET)
        } else {
            when (response.code) {
                400 -> networkEvent.publish(NetworkState.BAD_REQUEST)
                401 -> networkEvent.publish(NetworkState.UNAUTHORISED)
                403 -> networkEvent.publish(NetworkState.FORBIDDEN)
                404 -> networkEvent.publish(NetworkState.NOT_FOUND)
            }
        }

        return response
    }
}