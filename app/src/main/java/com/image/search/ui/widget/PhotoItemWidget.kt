package com.image.search.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.LinearLayoutCompat
import com.google.android.material.card.MaterialCardView
import com.image.search.R
import com.image.search.data.model.Photo
import com.image.search.utils.setImageWithUrl

class PhotoItemWidget @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : FrameLayout(context, attrs) {

    var ivThumbPhoto: ImageView
    var tvPhotographer: AppCompatTextView
    var tvAlt: AppCompatTextView

    init {
        LayoutInflater.from(context).inflate(R.layout.widget_item_image,this)
        ivThumbPhoto = findViewById(R.id.iv_thumb)
        tvPhotographer = findViewById(R.id.tv_photographer)
        tvAlt = findViewById(R.id.tv_alt)
    }

    fun applyData(photo: Photo){
        ivThumbPhoto.setImageWithUrl(photo.src.tiny)
        tvPhotographer.text = photo.photographer
        tvAlt.text = photo.alt
    }

}