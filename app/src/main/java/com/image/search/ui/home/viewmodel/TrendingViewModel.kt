package com.image.search.ui.home.viewmodel

import androidx.lifecycle.MutableLiveData
//import com.netviet.pharmacy.data.model.Trending
import com.image.search.ui.base.BaseViewModel

class TrendingViewModel : BaseViewModel(){

    private val points = MutableLiveData<String>()
    private val name = MutableLiveData<String>()

    fun setPoints(points: String){
        this.points.value = points
    }

    fun getPoints():MutableLiveData<String>{
        return points
    }

    fun setName(name: String){
        this.name.value = name
    }

    fun getName():MutableLiveData<String>{
        return name
    }
}