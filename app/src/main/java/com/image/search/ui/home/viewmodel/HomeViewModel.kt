package com.image.search.ui.home.viewmodel

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.provider.ContactsContract
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.image.search.data.model.Photo
import com.image.search.data.response.PhotosResponse
//import com.netviet.pharmacy.data.model.HomeDataEntity
import com.image.search.ui.base.BaseViewModel
import com.image.search.utils.constants.Constant
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


class HomeViewModel @Inject constructor() : BaseViewModel() {

    val homeLiveData = MutableLiveData<PhotosResponse>()

    fun loadHomeData(page : Int, keyword : String) {
        subscription = apiRepository.getImage(page,Constant.ITEM_PER_PAGE,keyword)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {photoResponse->
                homeLiveData.value = photoResponse
           //     Log.e("loadHomeData",bitmap.photos[0].photographer)
               // homeLiveData.value = bitmap
            }
    }
    fun loadImage() : Single<Bitmap>{
        return Single.fromCallable {
            val imageURL = "https://images.pexels.com/photos/14769376/pexels-photo-14769376.jpeg"
            val `in` = java.net.URL(imageURL).openStream()
            val image = BitmapFactory.decodeStream(`in`)
            image
        }
    }

//
//    private fun onRetrieveDataSuccess(homeData: HomeDataEntity?) {
//        homeData?.let {
//            Log.e("HOME_DATA",it.categoryList.toString())
//            homeLiveData.value = homeData
//        }
//    }


}