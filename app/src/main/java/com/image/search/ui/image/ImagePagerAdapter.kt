package com.image.search.ui.image

import android.app.Activity
import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.image.search.data.model.Photo


class ImagePagerAdapter(
    fragmentManager: FragmentManager,
    lifecycle: Lifecycle,
    private val photos: ArrayList<Photo>
) : FragmentStateAdapter(fragmentManager, lifecycle) {

    private val pageIds= photos.map { it.hashCode().toLong() }

    override fun getItemCount(): Int  = photos.size

    override fun createFragment(position: Int): Fragment {
        return ImageDetailFragment.newInstance(photos[position])
    }
    override fun getItemId(position: Int): Long {
        return photos[position].hashCode().toLong()
    }
    override fun containsItem(itemId: Long): Boolean {
        return pageIds.contains(itemId)
    }
    fun removeItem(activity: Activity, pos : Int){
        if (photos.isNotEmpty()){
            photos.removeAt(pos)
            notifyItemRemoved(pos)
            notifyItemRangeChanged(pos, photos.size)
        }else{
            activity.finish()
        }

    }

}