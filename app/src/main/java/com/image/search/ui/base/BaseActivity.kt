package com.image.search.ui.base

import android.os.Bundle
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.image.search.utils.network.NetworkEvent
import com.image.search.utils.network.NetworkState
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.functions.Consumer
import javax.inject.Inject

abstract class BaseActivity<T : ViewDataBinding, M : BaseViewModel> : DaggerAppCompatActivity() {

    protected lateinit var binding: T
    protected lateinit var viewModel: M

    @Inject
    protected lateinit var viewModelFactory: ViewModelProvider.Factory

    @LayoutRes
    protected abstract fun layoutRes(): Int

    protected abstract fun viewModelClass(): Class<M>

    protected abstract fun handleViewState()

    protected abstract fun initView()
    var savedInstanceState: Bundle? = null


//    abstract fun showLoading()
//
//    abstract fun hideLoading()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.savedInstanceState = savedInstanceState
        binding = DataBindingUtil.setContentView(this, layoutRes());
        binding.lifecycleOwner = this
        viewModel = ViewModelProvider(this, viewModelFactory).get(viewModelClass())
        initView()
        viewModel.viewState.observe(this, Observer { viewState ->
            viewState?.run {
                when (viewState) {
                    //ViewState.SHOW_LOADING -> showLoading()
                  //  ViewState.HIDE_LOADING -> hideLoading()
                    else -> handleViewState()
                }
            }
        })
    }

    /**
     * we register the BaseActivity as subscriber
     * and specify what needs to be done in case of NetworkState
     */
    override fun onResume() {
        super.onResume()


    }

    override fun onStop() {
        super.onStop()

    }

    override fun onDestroy() {
        super.onDestroy()
        NetworkEvent.unregister(this)
    }

}