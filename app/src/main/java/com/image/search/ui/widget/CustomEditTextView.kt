package com.image.netvietalarm.ui.wigget

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatEditText
import com.image.search.R
import com.image.search.utils.app.TypefaceProvider

class CustomEditTextView : AppCompatEditText {

    constructor(context: Context) : super(context) {
        initView(null)
    }
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initView(attrs)
    }
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initView(attrs)
    }


    private fun initView (attrs: AttributeSet?) {
        if(attrs != null){
            val typedValue : TypedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomEditTextView)
            val fontName : String? = typedValue.getString(R.styleable.CustomEditTextView_font_name)
            val typeFace : Typeface = TypefaceProvider.getTypeFace(context, fontName ?: "Montserrat-Regular")
            this.typeface = typeFace
            typedValue.recycle()
        }
    }



}