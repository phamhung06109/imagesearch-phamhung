package com.image.search.ui.home.adapter

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.image.search.data.model.Photo
import com.image.search.ui.home.HomeActivity
import com.image.search.ui.widget.PhotoItemWidget
import com.image.search.utils.BindableAdapter

class PhotosAdapter(private val photoListener : PhotoListener) : RecyclerView.Adapter<PhotosAdapter.PhotosViewHolder>(), BindableAdapter<List<Photo>> {

    private val photos = ArrayList<Photo>()


    class PhotosViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun applyData(photo : Photo){
            (itemView as PhotoItemWidget).applyData(photo)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotosViewHolder {
        return PhotosViewHolder(PhotoItemWidget(parent.context))
    }

    override fun onBindViewHolder(holder: PhotosViewHolder, position: Int) {
        holder.applyData(photos[position])
        holder.itemView.setOnClickListener {
            photoListener.onItemClick(position)
        }
    }

    override fun getItemCount(): Int = photos.size

    override fun setData(data: List<Photo>) {
        this.photos.clear()
        this.photos.addAll(data)
        notifyDataSetChanged()
    }
    fun applyData(data: List<Photo>) {
        this.photos.clear()
        this.photos.addAll(data)
        notifyDataSetChanged()
    }
    fun insertItem(data: List<Photo>, position : Int){
        this.photos.addAll(data)
        notifyItemInserted(position)
    }
    interface PhotoListener{
        fun onItemClick(position : Int)
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
    fun getPhotos() : ArrayList<Photo>{
        return photos
    }
    fun removeItem(pos : Int){
        if (photos.size>0){
            photos.removeAt(pos)
            notifyItemRemoved(pos)
            notifyItemRangeChanged(pos, photos.size)
        }

    }
}