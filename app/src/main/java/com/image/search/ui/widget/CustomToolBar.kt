package com.image.search.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.appcompat.widget.Toolbar
import com.image.search.R
import kotlinx.android.synthetic.main.custom_tool_bar.view.*

class CustomToolBar @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : Toolbar(context, attrs, defStyleAttr) {
    init {
        LayoutInflater.from(context).inflate(R.layout.custom_tool_bar,this)
        initView()
    }
    private fun initView(){
        ic_menu.setOnClickListener {
         //   RxBus.publish(
               // RxBus.TOOL_BAR_MENU_CLICK,"")
        }
    }
}