package com.image.search.ui.image

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.fragment.app.Fragment
import com.image.search.R
import com.image.search.data.model.Photo
import com.image.search.utils.setImageWithUrl

class ImageDetailFragment : Fragment() {

    companion object{
        fun newInstance(photo : Photo) : ImageDetailFragment{
            val fragment = ImageDetailFragment()
            val bundle = Bundle()
            bundle.putParcelable("photos",photo)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = LayoutInflater.from(requireContext()).inflate(R.layout.widget_fullscreen_image,container,false)
        val ivFullScreen = view.findViewById<AppCompatImageView>(R.id.iv_fullscreen)

        val photo = requireArguments().getParcelable<Photo>("photos")
        photo?.let {
            ivFullScreen.setImageWithUrl(it.src.portrait)
        }
        return view
    }
}