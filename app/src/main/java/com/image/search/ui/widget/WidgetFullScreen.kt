package com.image.search.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.appcompat.widget.AppCompatImageView
import com.image.search.R
import com.image.search.data.model.Photo
import com.image.search.utils.setImageWithUrl

class WidgetFullScreen @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : FrameLayout(context, attrs) {
    private var appCompatImageView : AppCompatImageView
    init {
        LayoutInflater.from(context).inflate(R.layout.widget_fullscreen_image,this)
        appCompatImageView = findViewById(R.id.iv_fullscreen)
    }
    fun applyData(photo: Photo){
        appCompatImageView.setImageWithUrl(photo.src.portrait)
    }
}