package com.image.search.ui.home

import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.image.search.R
import com.image.search.databinding.ActivityHomeBinding
import com.image.search.ui.base.BaseActivity
import com.image.search.ui.home.adapter.PhotosAdapter
import com.image.search.ui.home.viewmodel.HomeViewModel
import com.image.search.ui.image.ImageDetailActivity
import com.image.search.ui.widget.SearchWidget
import com.image.search.utils.PaginationScrollListener
import com.image.search.utils.app.rxjava.RxBus
import com.image.search.utils.constants.Constant

class HomeActivity : BaseActivity<ActivityHomeBinding, HomeViewModel>(), PhotosAdapter.PhotoListener, SearchWidget.OnSearchListener {

    private var isLastPage = false
    private var isLoading = false
    private var page = 1
    private var keyword = ""
    private var isTextChange = true
    private val adapter = PhotosAdapter(this)

    override fun layoutRes(): Int = R.layout.activity_home

    override fun viewModelClass(): Class<HomeViewModel> = HomeViewModel::class.java


    override fun initView() {
        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL,false)
        binding.widgetSearch.applyData(this)

        binding.rcPhotos.layoutManager = layoutManager

        binding.rcPhotos.adapter = adapter
        viewModel.homeLiveData.observe(this){
            if (isTextChange){
                adapter.applyData(it!!.photos)
            }else{
                adapter.insertItem(it!!.photos,page*Constant.ITEM_PER_PAGE-1)
            }
            val totalPage = it.totalResults/Constant.ITEM_PER_PAGE
            isLoading = false
            if (page > totalPage) {
                isLastPage = true
            } else {
                isLastPage = false
                page++
            }
        }

        binding.rcPhotos.addOnScrollListener(object : PaginationScrollListener(layoutManager) {
            override fun isLastPage(): Boolean = isLastPage

            override fun isLoading(): Boolean = isLoading

            override fun loadMoreItems() {
                isLoading = true
                isTextChange = false
                viewModel.loadHomeData(page, keyword)
            }

        })
        RxBus.subscribe(RxBus.TAG_REMOVE_CLICK,this){
            val pos = it as Int
            adapter.removeItem(pos)
            layoutManager.scrollToPositionWithOffset(pos,20)
        }
        RxBus.subscribe(RxBus.TAG_SCROLL,this){
            val position = it as Int
            layoutManager.scrollToPositionWithOffset(position,20)
        }

    }

    override fun onResume() {
        super.onResume()
        if (adapter.getPhotos().isEmpty()){
            binding.widgetSearch.resetSearch()
        }
    }

    override fun onItemClick(position: Int) {
        ImageDetailActivity.startActivity(this,adapter.getPhotos(),position)
    }

    override fun onTextChange(value: String) {
        isTextChange = true
        keyword = value
        viewModel.loadHomeData(page, keyword)
    }

    override fun handleViewState() {

    }
}