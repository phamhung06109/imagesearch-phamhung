package com.image.netvietalarm.ui.wigget

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatButton
import com.image.search.R

import com.image.search.utils.app.TypefaceProvider

class CustomButton : AppCompatButton {

    constructor(context: Context) : super(context) {
        initView(null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initView(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initView(attrs)
    }

    private fun initView(attrs: AttributeSet?) {
        if (attrs != null) {
            val typedValue: TypedArray =
                context.obtainStyledAttributes(attrs, R.styleable.CustomButton)
            val fontName: String? = typedValue.getString(R.styleable.CustomTextView_font_name)
            val typeFace: Typeface =
                TypefaceProvider.getTypeFace(context, fontName ?: "Montserrat-Regular")
            this.typeface = typeFace
            typedValue.recycle()
        }
    }

    fun setFontName(fontName: String) {
        val typeFace: Typeface = TypefaceProvider.getTypeFace(context, fontName)
        this.typeface = typeFace
    }


}