package com.image.search.ui.widget

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.view.isVisible
import com.image.search.R

class SearchWidget @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : FrameLayout(context, attrs) {

    var onSearchListener: OnSearchListener? = null

    private var linearLayoutCompat: LinearLayoutCompat
    private var edtSearch: AppCompatEditText

    init {
        LayoutInflater.from(context).inflate(R.layout.widget_search, this)
        linearLayoutCompat = findViewById(R.id.layout_place)
        edtSearch = findViewById(R.id.edt_search)


        edtSearch.onFocusChangeListener =
            OnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    linearLayoutCompat.visibility = View.GONE
                    edtSearch.visibility = View.VISIBLE
                }
            }


        edtSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                if (s != null) {
                    if (s.toString().isNotBlank() && s.toString().length > 1) {
                        onSearchListener?.onTextChange((s.toString()))
                    }
                }
            }
        })

    }

    fun resetSearch() {
        if (edtSearch.text != null) {
            edtSearch.text!!.clear()
        }
    }

    fun applyData(onSearchListener: OnSearchListener) {
        this.onSearchListener = onSearchListener
    }

    interface OnSearchListener {
        fun onTextChange(value: String)
    }
}