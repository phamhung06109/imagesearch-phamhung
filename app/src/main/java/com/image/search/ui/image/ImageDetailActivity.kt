package com.image.search.ui.image

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.viewpager2.widget.ViewPager2
import com.image.search.R
import com.image.search.data.model.Photo
import com.image.search.databinding.ActivityImageDetailsBinding
import com.image.search.ui.base.BaseActivity
import com.image.search.utils.app.rxjava.RxBus

class ImageDetailActivity : BaseActivity<ActivityImageDetailsBinding,ImageViewModel>() {

    private lateinit var imagePagerAdapter : ImagePagerAdapter
    private var pagePosition = 0

    companion object{
        fun startActivity(context : Context,photos: ArrayList<Photo>, position : Int){
            val intent = Intent(context,ImageDetailActivity::class.java)
            intent.putParcelableArrayListExtra("photos",photos)
            intent.putExtra("position", position)
            context.startActivity(intent)
        }
    }

    override fun layoutRes(): Int = R.layout.activity_image_details

    override fun viewModelClass(): Class<ImageViewModel> = ImageViewModel::class.java

    override fun handleViewState() {
    }

    override fun initView() {

        val photos = intent.getParcelableArrayListExtra<Photo>("photos")
        pagePosition = intent.getIntExtra("position",0)

        photos?.let {
            imagePagerAdapter = ImagePagerAdapter(supportFragmentManager, lifecycle, it)
            binding.mainPager.adapter = imagePagerAdapter
            binding.mainPager.setCurrentItem(pagePosition,true)
        }
        binding.mainPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                pagePosition = position
                RxBus.publish(RxBus.TAG_SCROLL,pagePosition)
            }
        })
       // Log.e("pagePosition",photos[].toString())
        binding.fab.setOnClickListener {

            RxBus.publish(RxBus.TAG_REMOVE_CLICK,pagePosition)
            imagePagerAdapter.removeItem(this,pagePosition)
        }
    }

}