package com.image.search.di.module

import com.image.search.data.repository.IApiRepository
import com.image.search.utils.constants.Constant
import com.image.search.utils.network.NetworkInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.ConnectionSpec
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module()
class NetworkModule{

    @Singleton
    @Provides
    fun provideRetrofit(client: OkHttpClient): Retrofit {
        //TODO fix here
        return Retrofit.Builder().baseUrl("https://api.pexels.com/v1/")
            .client(client)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Singleton
    @Provides
    fun provideApiService(retrofit: Retrofit): IApiRepository {
        return retrofit.create<IApiRepository>(IApiRepository::class.java)
    }
    @Provides
    @Singleton
    fun provideHttpClient(tokenInterceptor: NetworkInterceptor): OkHttpClient {
   //     sharedPreferences.getToken()
        val lists: List<ConnectionSpec> =
            listOf(ConnectionSpec.COMPATIBLE_TLS, ConnectionSpec.CLEARTEXT)

        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC)
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS)
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        return OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .addInterceptor(tokenInterceptor)
            .connectionSpecs(lists)
            .connectTimeout(Constant.DEFAULT_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(Constant.DEFAULT_TIMEOUT, TimeUnit.SECONDS)
            .build()
    }
}