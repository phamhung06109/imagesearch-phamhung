package com.image.search.di.component

import android.app.Application
import com.image.search.AppApplication
import com.image.search.di.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [ViewModelModule::class,NetworkModule::class, AppModule::class, AndroidSupportInjectionModule::class, ActivityBindingModule::class, FragmentModule::class])
interface AppComponent : AndroidInjector<AppApplication> {

    override fun inject(application: AppApplication)

    @Component.Builder
    interface Builder {


        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}