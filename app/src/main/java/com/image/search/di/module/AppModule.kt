package com.image.search.di.module

import android.app.Application
import android.content.Context
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class AppModule {

    @Binds
    @Singleton
    abstract fun provideContext(application: Application): Context
//
//    @Binds
//    @Singleton
//    abstract fun provideAppPreferences(appPreferences: SharedPreferences): SharedPreferences



}