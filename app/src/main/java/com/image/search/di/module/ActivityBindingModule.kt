package com.image.search.di.module

import com.image.search.ui.home.HomeActivity
import com.image.search.ui.image.ImageDetailActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ContributesAndroidInjector
    abstract fun bindImageDetailActivity(): ImageDetailActivity

    @ContributesAndroidInjector
    abstract fun bindHomeActivity(): HomeActivity

}