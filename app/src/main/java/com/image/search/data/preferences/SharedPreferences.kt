package com.image.search.data.preferences

import android.content.Context
import android.content.SharedPreferences
import javax.inject.Inject

class SharedPreferences @Inject constructor(private val context: Context) {

    private val USER_TOKEN = "user_token"
    private val TOKEN_TYPE = "token_type"

    private fun getPref(context : Context): SharedPreferences {
        //  return PreferenceManager.getDefaultSharedPreferences(context);
        return context.getSharedPreferences(
            context.packageName,
            Context.MODE_PRIVATE
        )
    }
    fun setToken(token : String) {
        val editor: SharedPreferences.Editor = getPref(context)
            .edit()
        editor.putString(USER_TOKEN, token)
        editor.apply()
    }

    fun getToken(): String {
        return getPref(context).getString(
            USER_TOKEN, "")!!
    }

    fun setTokenType(tokenType : String){
        val editor: SharedPreferences.Editor = getPref(context)
            .edit()
        editor.putString(TOKEN_TYPE, tokenType)
        editor.apply()
    }
    fun getTokenType(): String {
        return getPref(context).getString(
            TOKEN_TYPE, "")!!
    }

}