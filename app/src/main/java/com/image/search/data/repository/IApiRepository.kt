package com.image.search.data.repository

import com.image.search.data.response.*
import io.reactivex.Observable
import retrofit2.http.*

interface IApiRepository {

    @GET("search")
    fun getImage(@Query("page") page : Int, @Query("per_page") perPage : Int, @Query("query") query : String): Observable<PhotosResponse>
}