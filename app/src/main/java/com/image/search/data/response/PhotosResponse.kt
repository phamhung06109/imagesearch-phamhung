package com.image.search.data.response

import com.google.gson.annotations.SerializedName
import com.image.search.data.model.Photo

data class PhotosResponse(
    @SerializedName("total_results")
    val totalResults: Int,
    val page: Int,
    @SerializedName("per_page")
    val perPage: Int,
    val photos: List<Photo>,
    @SerializedName("next_page")
    val nextPage: String
) {

}